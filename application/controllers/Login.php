<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_login', 'myModel');
    }
    
    public function index()
    {
        $data['title'] = 'Login Page';
        $this->load->view('login', $data, FALSE);
    }

    public function do_login()
    {
        $param = $this->input->post();
        $proses = $this->myModel->getUser($param);
                
        if ($proses) {
            $this->session->set_userdata('userlogin', $proses[0]);
            redirect('arsip_masuk');
        } else {
            $this->session->set_flashdata('err_message', 'Username of Password is Incorrect, Please Try Again.');
            redirect('login');
        }
    }

    public function do_logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}

/* End of file Login.php */
