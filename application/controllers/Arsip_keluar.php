<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Arsip_keluar extends ADMIN_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_arsip_k', 'myModel');
    }
    
    public function index()
    {
        $data['titleHead'] = 'Arsip Keluar';
        $data['lists'] = $this->myModel->getData();
        
        $this->template->render('pages/arsip_keluar/index', $data, FALSE);
    }

    public function form_input()
    {
        $data['title'] = 'Tambah Data';
        $this->template->render('pages/arsip_keluar/form_input', $data, FALSE);
    }

    public function input()
    {
        $data = $this->input->post();
        $this->myModel->inputData($data);
        
        redirect('arsip_keluar');
    }

    public function form_edit()
    {
        $id = $this->uri->segment(3);
        
        $data['title'] = 'Form Edit';
        $data['detail'] = $this->myModel->getDataByID($id);
        $this->template->render('pages/arsip_keluar/form_edit', $data, FALSE);
    }

    public function edit()
    {
        $data = $this->input->post();     
        $this->myModel->editData($data);

        redirect('arsip_keluar');
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->myModel->deleteData($id);
        
        redirect('arsip_keluar');
    }
}

/* End of file Home.php */
