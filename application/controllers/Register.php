<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class Register extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_register', 'myModel');
    }
    
    public function index()
    {
        $this->load->view('register');   
    }

    public function do_register()
    {
        $data = $this->input->post();
        $uuid1 = Uuid::uuid1();
        $uuid4 = Uuid::uuid4();

        $data['id'] = $uuid1->toString();
        $data['id_petugas'] = $data['id'];
        $data['id_user'] = $uuid4->toString();
        
        $this->myModel->register($data);

        redirect('login','refresh');
        
    }

}

/* End of file Register.php */
