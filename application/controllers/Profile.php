<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_petugas', 'myModel');
    }
    
    public function index()
    {
        $list = $this->myModel->getProfile();
        
        $data['titleHead'] = 'Profile';
        $data['user'] = $list['user'];
        $data['petugas'] = $list['petugas'];

        $this->template->render('pages/profile/index', $data);
    }

}

/* End of file Profile.php */
