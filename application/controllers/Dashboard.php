<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends ADMIN_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_arsip_m', 'mArsipM');
        $this->load->model('M_arsip_k', 'mArsipK');
        $this->load->model('M_petugas', 'mPetugas');        
    }
    

    public function index()
    {
        $sortBy = [
            'masuk' => 'tgl_msk',
            'keluar' => 'tgl_keluar'
        ];
        $data['titleHead'] = 'Dashboard';
        // $data['petugas'] = $this->mPetugas->getNum(); 
        $data['listPetugas'] = $this->mPetugas->getData();
        // $data['arsipKeluar'] = $this->mArsipK->getNum();
        $data['listKeluar'] = $this->mArsipK->getData($sortBy['keluar']);
        // $data['arsipMasuk'] = $this->mArsipM->getNum();
        $data['listMasuk'] = $this->mArsipM->getData($sortBy['masuk']);
        $this->template->render('pages/dashboard/index', $data);        
    }

}

/* End of file Dashboard.php */
