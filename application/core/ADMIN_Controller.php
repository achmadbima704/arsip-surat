<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ADMIN_Controller extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        
        $userlogin = $this->session->userdata('userlogin');
        if (@$userlogin->id == '') {
            redirect('login','refresh');
        }
        
    }
    
    public function index()
    {
        
    }

}

/* End of file ADMIN_Controller.php */
