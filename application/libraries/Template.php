<?php 
class Template
{
    
    public function __construct() {
        $this->ci =& get_instance();
    }

    public function render($path = null, $data = [])
    {
        $data['_meta'] = $this->ci->load->view('pages/layouts/_meta', $data, TRUE);
        $data['_stylesheet'] = $this->ci->load->view('pages/layouts/_stylesheet', $data, TRUE);
        $data['_navigation'] = $this->ci->load->view('pages/layouts/_navigation', $data, TRUE);
        $data['_script'] = $this->ci->load->view('pages/layouts/_script', $data, TRUE);
        $data['_page'] = $this->ci->load->view($path, $data, TRUE);

        $this->ci->load->view('pages/layouts/_template', $data, FALSE);
        
    }

}
