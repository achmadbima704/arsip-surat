<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

    public function getUser($param)
    {
        $username = $param['username'];
        $password = md5($param['password']);

        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $data = $this->db->get('users');

        return $data->result();
    }

}

/* End of file M_login.php */
