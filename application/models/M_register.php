<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_register extends CI_Model {

    public function register($data)
    {
        
        // echo "<pre>";
        // print_r ($data);
        // echo "</pre>";exit();
        
        $objectPetugas = [
            'id' => $data['id'],
            'email' => $data['email'],
            'nama' => $data['nama'],
            'agama' => $data['agama'],
            'domisili' => $data['domisili'],
            'alamat' => $data['alamat'],
            'jk' => $data['jk'],
            'no_telp' => $data['no_telp']
        ];
        $this->db->insert('petugas', $objectPetugas);
        
        $objectUser = [
            'id' => $data['id_user'],
            'username' => $data['username'],
            'password' => md5($data['password']),
            'id_petugas' => $data['id_petugas']
        ];
        $this->db->insert('users', $objectUser);
    }

}

/* End of file M_register.php */
