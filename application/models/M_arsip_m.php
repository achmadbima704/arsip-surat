<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class M_arsip_m extends CI_Model {

    public function getData($sortBy = 'id')
    {       
        if ($sortBy == 'id') {
            $urut = 'ASC';
        } else {
            $urut = "DESC";
        }

        $this->db->select('arsip_masuk.id, pengirim, perihal, penerima, tgl_msk, petugas.nama');
        $this->db->join('petugas', 'petugas.id = arsip_masuk.id_petugas');
        $this->db->order_by($sortBy, $urut);
        $data = $this->db->get('arsip_masuk');
    
        return $data->result();
    }

    public function getDataByID($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->get('arsip_masuk')
                        ->result();
        return $data[0];
    }

    public function inputData($data)
    {
        $data['id'] = Uuid::uuid1();
        $id_petugas = $this->session->userdata('userlogin');
        
        $object = [
            'pengirim' => $data['pengirim'],
            'perihal' => $data['perihal'],
            'penerima' => $data['penerima'],
            'tgl_msk' => $data['tgl_msk'],
            'id_petugas' => $id_petugas->id_petugas
        ];

        $this->db->insert('arsip_masuk', $object);
    }

    public function editData($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('arsip_masuk', $data);
    }

    public function deleteData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('arsip_masuk');
    }

}

/* End of file M_arsip_m.php */
