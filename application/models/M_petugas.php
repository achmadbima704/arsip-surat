<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_petugas extends CI_Model {

    public function getData()
    {
        $data = $this->db->get('petugas');
        return $data->result();
    }

    public function getProfile()
    {
        $idUser = $this->session->userdata('userlogin');
        
        $this->db->where('id', $idUser->id);
        $user = $this->db->get('users')->result();
        $data['user'] = $user[0];

        $this->db->where('id', $idUser->id_petugas);
        $petugas = $this->db->get('petugas')->result();
        $data['petugas'] = $petugas[0];

        return $data;                                    
    }
}

/* End of file M_petugas.php */
