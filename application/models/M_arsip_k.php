<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class M_arsip_k extends CI_Model {

    public function getData($sortBy = 'id')
    {   
        if ($sortBy == 'id') {
            $urut = 'ASC';
        } else {
            $urut = "DESC";
        }

        $this->db->select('arsip_keluar.id, no_surat, tujuan, perihal, departemen, tgl_keluar, petugas.nama');
        $this->db->join('petugas', 'petugas.id = arsip_keluar.id_petugas');
        $this->db->order_by($sortBy, $urut);
        $data = $this->db->get('arsip_keluar');
    
        return $data->result();
    }

    public function getDataByID($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->get('arsip_keluar')
                        ->result();
        return $data[0];
    }

    public function inputData($data)
    {
        $data['id'] = Uuid::uuid1();
        $id_petugas = $this->session->userdata('userlogin');
        
        $object = [
            'no_surat' => $data['no_surat'],
            'perihal' => $data['perihal'],
            'tujuan' => $data['tujuan'],
            'tgl_keluar' => $data['tgl_keluar'],
            'departemen' => $data['departemen'],
            'id_petugas' => $id_petugas->id_petugas
        ];

        $this->db->insert('arsip_keluar', $object);
    }

    public function editData($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('arsip_keluar', $data);
    }

    public function deleteData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('arsip_keluar');
    }

}

/* End of file M_arsip_k.php */
