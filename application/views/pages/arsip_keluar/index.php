<h4>
    <?php echo $titleHead ?>
</h4>

<a class="btn btn-success" href="<?php echo site_url('arsip_keluar/form_input') ?>" role="button"><i class="fas fa-plus-square"></i></a>
<table class="table table-hover mt-3">
    <thead>
        <tr>
            <th scope="col">No. Surat</th>
            <th scope="col">Tujuan</th>
            <th scope="col">Perihal</th>
            <th scope="col">Departemen</th>
            <th scope="col">Tgl. Keluar</th>
            <th scope="col">Petugas</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($lists as $key => $value) { ?>
        <tr>
            <td scope="row">
                <?php echo $value->no_surat ?>
            </td>
            <td scope="row">
                <?php echo $value->tujuan ?>
            </td>
            <td scope="row">
                <?php echo $value->perihal ?>
            </td>
            <td scope="row">
                <?php echo $value->departemen ?>
            </td>
            <td scope="row">
                <?php echo $value->tgl_keluar ?>
            </td>
            <td scope="row">
                <?php echo $value->nama ?>
            </td>
            <td scope="row">
                <a class="waves-effect waves-light yellow darken-2 btn" href="<?php echo site_url('arsip_keluar/form_edit/' . $value->id) ?>" role="button"><i class="fas fa-pen-square"></i></a>
                <a class="waves-effect waves-light red darken-1 btn" href="<?php echo site_url('arsip_keluar/delete/' . $value->id) ?>" role="button"><i class="fas fa-trash-alt"></i></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>