<h4>
    <?php echo $title ?>
</h4>

<form class="mt-4" action="<?php echo site_url('arsip_keluar/input') ?>" method="post">
    <div class="form-group">
        <label for="no_surat">No. Surat</label>
        <input class="form-control" type="text" name="no_surat" id="no_surat">
    </div>
    <div class="form-group">
        <label for="perihal">Perihal</label>
        <input class="form-control" type="text" name="perihal" id="perihal">
    </div>
    <div class="form-group">
        <label for="tujuan">Tujuan</label>
        <input class="form-control" type="text" name="tujuan" id="tujuan">
    </div>
    <div class="form-group">
        <label for="departemen">Departemen</label>
        <input class="form-control" type="text" name="departemen" id="departemen">
    </div>

    <input type="hidden" name="tgl_keluar" value="<?php echo date('y/m/d') . ' '; echo date('h:i:s')?>">

    <button class="btn btn-primary" type="submit">Simpan</button>
</form>