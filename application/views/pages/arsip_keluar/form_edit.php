<h4>
    <?php echo $title?>
</h4>

<form action="<?php echo site_url('arsip_keluar/edit')?>" method="post">
    <div class="form-group">
        <label for="no_surat">No. Surat</label>
        <input class="form-control" type="text" name="no_surat" id="no_surat" value="<?php echo $detail->no_surat ?>">
    </div>
    <div class="form-group">
        <label for="perihal">Perihal</label>
        <input class="form-control" type="text" name="perihal" id="perihal" value="<?php echo $detail->perihal ?>">
    </div>
    <div class="form-group">
        <label for="tujuan">Tujuan</label>
        <input class="form-control" type="text" name="tujuan" id="tujuan" value="<?php echo $detail->tujuan ?>">
    </div>
    <div class="form-group">
        <label for="departemen">Departemen</label>
        <input class="form-control" type="text" name="departemen" id="departemen" value="<?php echo $detail->departemen?>">
    </div>
    <input type="hidden" name="id" value="<?php echo $detail->id?>">
    <button class="btn btn-primary" type="submit">Simpan</button>
</form>