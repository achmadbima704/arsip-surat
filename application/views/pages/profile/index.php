<?php
    if ($petugas->jk == 'L') {
        $jk = 'Laki - Laki';
    } else {
        $jk = "Perempuan";
    }
?>
<h4> <?php echo $titleHead ?> </h4>
<div class="row">
    <div class="input-field col s12">
        <input disabled value="<?php echo $petugas->nama ?>" id="nama" type="text" class="validate">
        <label for="nama">Nama</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <input disabled value="<?php echo $user->username ?>" id="username" type="text" class="validate">
        <label for="username">Username</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <input disabled value="<?php echo $jk ?>" id="jk" type="text" class="validate">
        <label for="jk">Jenis Kelamin</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <input disabled value="<?php echo $petugas->alamat ?>" id="alamat" type="text" class="validate">
        <label for="alamat">Alamat</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <input disabled value="<?php echo $petugas->domisili ?>" id="domisili" type="text" class="validate">
        <label for="domisili">Domisili</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <input disabled value="<?php echo $petugas->agama ?>" id="agama" type="text" class="validate">
        <label for="agama">Agama</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <input disabled value="<?php echo $petugas->email ?>" id="email" type="text" class="validate">
        <label for="email">Email</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <input disabled value="<?php echo $petugas->no_telp ?>" id="no_telp" type="text" class="validate">
        <label for="no_telp">No. Telp</label>
    </div>
</div>