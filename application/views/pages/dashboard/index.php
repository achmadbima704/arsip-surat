<h4>Surat keluar terakhir</h4>

<table class="table table-hover mt-4 mb-4">
    <thead>
        <tr>
            <th scope="col">No. Surat</th>
            <th scope="col">Tujuan</th>
            <th scope="col">Perihal</th>
            <th scope="col">Departemen</th>
            <th scope="col">Tgl. Keluar</th>
            <th scope="col">Petugas</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listKeluar as $key => $value) { ?>
        <tr>
            <td scope="row">
                <?php echo $value->no_surat ?>
            </td>
            <td scope="row">
                <?php echo $value->tujuan ?>
            </td>
            <td scope="row">
                <?php echo $value->perihal ?>
            </td>
            <td scope="row">
                <?php echo $value->departemen ?>
            </td>
            <td scope="row">
                <?php echo $value->tgl_keluar ?>
            </td>
            <td scope="row">
                <?php echo $value->nama ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<hr>

<h4 class="mt-4">Surat masuk terakhir</h4>
<table class="table table-hover mt-4">
    <thead>
        <tr>
            <th scope="col">Pengirim</th>
            <th scope="col">Perihal</th>
            <th scope="col">Penerima</th>
            <th scope="col">Tanggal Masuk</th>
            <th scope="col">Petugas</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listMasuk as $key => $value) { ?>
        <tr>
            <td scope="row">
                <?php echo $value->pengirim?>
            </td>
            <td scope="row">
                <?php echo $value->perihal?>
            </td>
            <td scope="row">
                <?php echo $value->penerima?>
            </td>
            <td scope="row">
                <?php echo $value->tgl_msk?>
            </td>
            <td scope="row">
                <?php echo $value->nama?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>