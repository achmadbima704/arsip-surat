<!DOCTYPE html>
<html lang="en">

<head>
    <?php echo $_meta ?>
    <?php echo $_stylesheet ?>
    <title>Admin Page</title>
</head>

<body>
    <div class="wrapper">
        <?php echo $_navigation ?>
        <div class="adm-content">
            <!-- awal content -->
            <div class="content">
                <?php echo $_page ?>
            </div>
            <!-- akhir content -->
        </div>
    </div>
    <?php echo $_script ?>
</body>

</html>