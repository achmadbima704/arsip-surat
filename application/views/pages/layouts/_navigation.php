<!-- awal navigation -->
<?php 
    $page = $this->uri->segment(1);
?>
<div class="adm-sidebar">
    <h4>Admin Page</h4>
    <div class="menu">
        <ul>
            <li class="<?php if($page == 'dashboard'){ echo "active"; } ?>"><i class="fas fa-tachometer-alt"><a href="<?php echo site_url('dashboard') ?>"></i> Dashboard</a></li>
            <li class="<?php if($page == 'arsip_masuk'){ echo "active"; } ?>"><i class="fas fa-list-ul"></i><a href="<?php echo site_url('arsip_masuk') ?>"> Arsip Masuk</a></li>
            <li class="<?php if($page == 'arsip_keluar'){ echo "active"; } ?>"><i class="fas fa-list-ul"></i><a href="<?php echo site_url('arsip_keluar') ?>"> Arsip Keluar</a></li>
            <li class="<?php if($page == 'profile'){ echo "active"; } ?>"><i class="fas fa-user"></i><a href="#"> Profile</a></li>
            <li><a href="<?php echo base_url('login/do_logout') ?>"><i class="fas fa-power-off"></i> Log Out</a></li>
        </ul>
    </div>
</div>
<div class="adm-top-nav">
    <h5><?php echo $titleHead ?></h5>
</div>
<!-- akhir navigation -->