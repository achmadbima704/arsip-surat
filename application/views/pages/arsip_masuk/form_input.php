<h4>
    <?php echo $title ?>
</h4>

<form action="<?php echo site_url('arsip_masuk/input') ?>" method="post">
    <div class="form-group">
        <label for="pengirim">Pengirim</label>
        <input class="form-control" type="text" name="pengirim" id="pengirim">
    </div>
    <div class="form-group">
        <label for="perihal">Perihal</label>
        <input class="form-control" type="text" name="perihal" id="perihal">
    </div>
    <div class="form-group">
        <label for="penerima">Departemen</label>
        <input class="form-control" type="text" name="penerima" id="penerima">
    </div>
    <input type="hidden" name="tgl_msk" value="<?php echo date('y/m/d') . ' '; echo date('h:i:s')?>">
    <button class="btn btn-primary" type="submit">Simpan</button>
</form>