<h4>
    <?php echo $title?>
</h4>

<form action="<?php echo site_url('arsip_masuk/edit')?>" method="post">
    <div class="form-group">
        <label for="pengirim">Pengirim</label>
        <input class="form-control" type="text" name="pengirim" id="pengirim" value="<?php echo $detail->pengirim ?>">
    </div>
    <div class="form-group">
        <label for="perihal">Perihal</label>
        <input class="form-control" type="text" name="perihal" id="perihal" value="<?php echo $detail->perihal ?>">
    </div>
    <div class="form-group">
        <label for="penerima">Departemen</label>
        <input class="form-control" type="text" name="penerima" value="<?php echo $detail->penerima ?>">
    </div>
    <input type="hidden" name="id" value="<?php echo $detail->id?>">
    <button class="btn btn-primary" type="submit">Simpan</button>
</form>