<h4>
    <?php echo $titleHead ?>
</h4>
<a class="btn btn-success" href="<?php echo site_url('arsip_masuk/form_input') ?>" role="button"> <i class="fas fa-plus-square"></i></a>
<table class="table table-hover mt-3">
    <thead>
        <tr>
            <th scope="col">Pengirim</th>
            <th scope="col">Perihal</th>
            <th scope="col">Departemen</th>
            <th scope="col">Panggal Masuk</th>
            <th scope="col">Petugas</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($lists as $key => $value) { ?>
        <tr>
            <td scope="row">
                <?php echo $value->pengirim?>
            </td>
            <td scope="row">
                <?php echo $value->perihal?>
            </td>
            <td scope="row">
                <?php echo $value->penerima?>
            </td>
            <td scope="row">
                <?php echo $value->tgl_msk?>
            </td>
            <td scope="row">
                <?php echo $value->nama?>
            </td>
            <td scope="row">
                <a class="waves-effect waves-light yellow darken-2 btn" href="<?php echo site_url('arsip_masuk/form_edit/' . $value->id) ?>"><i class="fas fa-pen-square"></i></a>
                <a class="waves-effect waves-light red darken-1 btn" href="<?php echo site_url('arsip_masuk/delete/' . $value->id) ?>"><i class="fas fa-trash-alt"></i></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>