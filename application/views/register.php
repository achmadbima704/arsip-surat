<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url('node_modules/materialize-css/dist/css/materialize.min.css')?>">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css"> -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
    <title>Form Register</title>
</head>

<body>
    <h4 id="register-head">Form Register</h4>
    <div id="register-page" class="row">
        <div class="col s12 z-depth-6 card-panel">
            <form class="register-form" action="<?php echo site_url('register/do_register')?>" method="post">
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="username" type="text" name="username" class="validate">
                        <label for="username" class="center-align">Username</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="email" type="email" name="email" class="validate">
                        <label for="email" class="center-align">Email</label>
                    </div>
                </div>
                <div class="row margin">
				    <div class="input-field col s12">
					    <input id="user_passw" type="password" name="password" class="validate">
					    <label for="user_passw">Password</label>
				    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="nama" type="text" name="nama">
                        <label for="nama">nama</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="domisili" type="text" name="domisili">
                        <label for="domisili">Domisili</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="agama" type="text" name="agama">
                        <label for="agama">Agama</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="alamart" type="text" name="alamat">
                        <label for="alamat">Alamat</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="jk" type="text" name="jk">
                        <label for="jk">Jenis Kelamin</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="no_telp" type="text" name="no_telp">
                        <label for="no_telp">No. Telp</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button type="submit" class="btn waves-effect waves-light col s12">Register Now</button>
                    </div>
                    <div class="input-field col s12">
                        <p class="margin center medium-small sign-up">Already have an account? <a href="login.php">Login</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="<?php echo base_url('node_modules/materialize-css/dist/js/materialize.min.js') ?>"></script>
</body>

</html>