<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
  <link rel="icon" href="img/microsoft_PNG9.png" type="image/png" sizes="16x16">
  <title>Login Page</title>
</head>
<body>
  <div class="lgn-container">
    <div class="bg-login">
      <img src="<?php echo base_url('assets/img/avi-richards-183715-unsplash.jpg')  ?>" alt="">
    </div>
    <div class="form-container">
    <?=$this->session->flashdata('err_message');?>
      <form class="form-body" action="<?php echo site_url('login/do_login') ?>" method="post">
        <h1>Aplikasi Arsip</h1>
        <h3>Log in</h3>
        <input type="text" name="username" placeholder="Username" required>
        <input type="password" name="password" placeholder="Password" required>
        <button type="submit">Log In</button>
        <span class="custom-control custom-checkbox"><input type="checkbox" name="remember"> Remember me |</span>
        <span><a href="<?php echo site_url('register') ?>">Belum punya akun?</a></span>
      </form>
    </div>
  </div>
</body>
</html>